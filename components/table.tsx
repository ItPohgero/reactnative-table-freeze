import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

interface TableProps {
  style?: object;
  borderStyle?: any;
  children: React.ReactNode;
}

const Table: React.FC<TableProps> = ({ style, borderStyle, children }) => {
  const borderLeftWidth = (borderStyle && borderStyle.borderWidth) || 0;
  const borderBottomWidth = borderLeftWidth;
  const borderColor = (borderStyle && borderStyle.borderColor) || '#000';

  return (
    <View
      style={[
        style,
        {
          borderLeftWidth,
          borderBottomWidth,
          borderColor,
        },
      ]}
    >
      {children}
    </View>
  );
};

Table.propTypes = {
  style: PropTypes.object,
  borderStyle: PropTypes.object,
  children: PropTypes.node.isRequired,
};

interface TableWrapperProps {
  style?: object;
  children: React.ReactNode;
}

const TableWrapper: React.FC<TableWrapperProps> = ({ style, children }) => {
  return <View style={style}>{children}</View>;
};

TableWrapper.propTypes = {
  style: PropTypes.object,
  children: PropTypes.node.isRequired,
};

export { Table, TableWrapper };
