import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { sum } from '../utils';
import Cell from './cell';

interface RowProps {
  style?: object;
  textStyle?: object;
  widthArr?: Array<number>;
  height?: number;
  flexArr?: Array<number>;
  data?: Array<React.ReactNode>;
  cellTextStyle?: (item: React.ReactNode) => object;
}

const Row: React.FC<RowProps> = ({
  data,
  style,
  widthArr,
  height,
  flexArr,
  textStyle,
  cellTextStyle,
  ...props
}) => {
  let width = widthArr ? sum(widthArr) : 0;

  return data ? (
    <View style={[height && { height }, width && { width }, styles.row, style]}>
      {data.map((item, i) => {
        const flex = flexArr && flexArr[i];
        const wth = widthArr && widthArr[i];
        return (
          <Cell
            key={i}
            data={item}
            width={wth}
            height={height}
            flex={flex}
            textStyle={[cellTextStyle && cellTextStyle(item), textStyle]}
            {...props}
          />
        );
      })}
    </View>
  ) : null;
};

interface RowsProps {
  style?: object;
  textStyle?: object;
  widthArr?: Array<number>;
  heightArr?: Array<number>;
  flexArr?: Array<number>;
  data?: Array<Array<React.ReactNode>>;
  cellTextStyle?: (item: React.ReactNode) => object;
}

const Rows: React.FC<RowsProps> = ({
  data,
  style,
  widthArr,
  heightArr,
  flexArr,
  textStyle,
  cellTextStyle,
  ...props
}) => {
  const flex = flexArr ? sum(flexArr) : 0;
  const width = widthArr ? sum(widthArr) : 0;

  return data ? (
    <View style={[flex && { flex }, width && { width }]}>
      {data.map((item, i) => {
        const height = heightArr && heightArr[i];
        return (
          <Row
            key={i}
            data={item}
            widthArr={widthArr}
            height={height}
            flexArr={flexArr}
            style={style}
            textStyle={textStyle}
            cellTextStyle={cellTextStyle}
            {...props}
          />
        );
      })}
    </View>
  ) : null;
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    overflow: 'hidden',
  },
});

export { Row, Rows };
