import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { sum } from '../utils';
import Cell from './cell';

interface ColProps {
  width?: number;
  style?: object;
  textStyle?: object;
  data?: Array<React.ReactNode>;
  heightArr?: Array<number>;
  flex?: number;
}

const Col: React.FC<ColProps> = ({
  data,
  style,
  width,
  heightArr,
  flex,
  textStyle,
  ...props
}) => {
  return data ? (
    <View
      style={[width ? { width: width } : { flex: 1 }, { flex: flex ?? 0 }, style]}>
      {data.map((item, i) => {
        const height = heightArr && heightArr[i];
        return (
          <Cell
            key={i}
            data={item}
            width={width}
            height={height}
            textStyle={textStyle}
            {...props}
          />
        );
      })}
    </View>
  ) : null;
};

interface ColsProps {
  style?: object;
  textStyle?: object;
  data?: Array<Array<React.ReactNode>>;
  widthArr?: Array<number>;
  heightArr?: Array<number>;
  flexArr?: Array<number>;
}

const Cols: React.FC<ColsProps> = ({
  data,
  style,
  widthArr,
  heightArr,
  flexArr,
  textStyle,
  ...props
}) => {
  let width = widthArr ? sum(widthArr) : 0;

  return data ? (
    <View style={[styles.cols, width && { width }]}>
      {data.map((item, i) => {
        const flex = flexArr && flexArr[i];
        const wth = widthArr && widthArr[i];
        return (
          <Col
            key={i}
            data={item}
            width={wth}
            heightArr={heightArr}
            flex={flex}
            style={style}
            textStyle={textStyle}
            {...props}
          />
        );
      })}
    </View>
  ) : null;
};

const styles = StyleSheet.create({
  cols: { flexDirection: 'row' },
});

export { Col, Cols };
