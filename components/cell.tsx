import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

interface CellProps {
  data: React.ReactNode;
  width?: number | string;
  height?: number | string;
  flex?: number;
  style?: object;
  textStyle?: object;
  borderStyle?: {
    borderWidth?: number;
    borderColor?: string;
  };
}

const Cell: React.FC<CellProps> = ({
  data,
  width,
  height,
  flex,
  style,
  textStyle,
  borderStyle,
  ...props
}) => {
  const textDom = React.isValidElement(data) ? (
    data
  ) : (
    <Text style={[textStyle, styles.text]} {...props}>
      {data}
    </Text>
  );
  const borderTopWidth = (borderStyle && borderStyle.borderWidth) || 0;
  const borderRightWidth = borderTopWidth;
  const borderColor = (borderStyle && borderStyle.borderColor) || '#000';

  return (
    <View
      style={[
        {
          borderTopWidth,
          borderRightWidth,
          borderColor,
        },
        styles.cell,
        { width },
        { height },
        { flex },
        !width && !flex && !height && !style && { flex: 1 },
        style,
      ]}
    >
      {textDom}
    </View>
  );
};

Cell.propTypes = {
  data: PropTypes.node.isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  flex: PropTypes.number,
  style: PropTypes.object,
  textStyle: PropTypes.object,
  borderStyle: PropTypes.object,
};

const styles = StyleSheet.create({
  cell: { justifyContent: 'center' },
  text: { backgroundColor: 'transparent' },
});

export default Cell;
