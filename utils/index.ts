export const sum = (arr: any[]) => arr.reduce((acc: any, n: any) => acc + n, 0);
