import { TextStyle, ViewStyle } from "react-native";

const borderColor = '#f1f5f9';
const SLATE100 = '#f1f5f9';
const HEADER_HIGHT = 30;

const styling = {
    container: {
        marginTop: 60,
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#eee',
    } as ViewStyle,

    // HEAD
    boxTopLeft: {
        flexDirection: 'row',
        height: HEADER_HIGHT + 3,
        backgroundColor: "#cbd5e1",
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    } as ViewStyle,
    boxHead: {
        height: HEADER_HIGHT,
        backgroundColor: '#cbd5e1',
        color: '#64748b'
    } as ViewStyle,

    // COLUMN
    columnLeft: {
        flex: 1,
        backgroundColor: 'white',
    } as ViewStyle,
    columnLeftBorder: {
        borderWidth: 1,
        borderColor,
    } as ViewStyle,

    // ROW
    rowContainer: {
        flex: 1,
        backgroundColor: 'white',
    } as ViewStyle,
    rowOdd: {
        height: 32,
        backgroundColor: SLATE100
    } as ViewStyle,
    rowEven: {
        height: 32,
    } as ViewStyle,
    textStyle: {
        textAlign: 'center', color: '#64748b'
    } as ViewStyle,
    textStyleSalesman: {
        textAlign: 'left', color: '#64748b', paddingLeft: 6
    } as TextStyle,
}

export default styling