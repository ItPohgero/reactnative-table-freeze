import React, { useRef } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import styling from './App.style';
import { data } from './data';
import { Table } from './components/table';
import { Row } from './components/rows';

const BORDER_COLOR = '#f1f5f9';

export default function TableDemo() {
  const leftRef = useRef<ScrollView>(null);
  const rightRef = useRef<ScrollView>(null);

  const coreTableHead = {
    tableHead: [
      'TGT',
      'ACT',
      '%',
      'TGT',
      'ACT',
      '%',
    ],
    widthArr: [60, 60, 60, 100, 100, 60],
  };

  const salesman = (e: string) => {
    return (
      <Text style={styling.textStyleSalesman}>{e}</Text>
    )
  }

  const [isCollapse, SetIsCollapse] = React.useState<number>();
  const collapse = (e: number) => {
    return (
      <TouchableOpacity
        style={{ flexDirection: 'row', 'justifyContent': 'center', alignItems: 'center' }}
        onPress={() => {
          if (isCollapse === e) {
            SetIsCollapse(0)
          } else {
            SetIsCollapse(e)
          }
        }}>
        <Text style={styling.textStyleSalesman}>{isCollapse !== e ? "+" : "-"} {e}</Text>
      </TouchableOpacity>
    )
  }

  // mapping freeze data
  const tableDataFreeze: (string | number | React.JSX.Element)[][] = data.map((item: any, index: number) => [
    collapse(index + 1),
    salesman(`${item.salesman?.slice(0, 10)}...`),
  ]);

  // mapping core data
  const tableDataCore: any = data.map((item: any) => [
    item.qty_tgt,
    item.qty_real,
    item.qty_percentage,
    item.value_tgt,
    item.value_real,
    item.value_percentage,
    // tambah data diatas sini
    item.last_month,
    item.last_three_month
  ]);

  return (
    <View style={styling.container}>
      <View>
        <View style={[styling.boxTopLeft]}>
          <Text style={{ color: 'white', width: 60, textAlign: 'center' }} />
          <Text style={{ color: 'white', width: 100, textAlign: 'center' }} />
        </View>
        <View style={[styling.boxTopLeft, { marginTop: -6 }]}>
          <Text style={{ color: '#64748b', width: 60, textAlign: 'center' }}>No</Text>
          <Text style={{ color: '#64748b', width: 100, textAlign: 'center' }}>Salesman</Text>
        </View>
        <ScrollView
          ref={leftRef}
          style={styling.columnLeft}
          scrollEnabled={false}
          showsVerticalScrollIndicator={false}
        >

          <Table borderStyle={styling.columnLeftBorder}>
            {tableDataFreeze.map((d, k) => (
              <>
                <Row
                  key={k}
                  data={d}
                  widthArr={[60, 100]}
                  style={k % 2 ? styling.rowEven : styling.rowOdd}
                  textStyle={styling.textStyle}
                />
                {isCollapse === (k + 1) && (
                  <View style={{ height: 65, flexDirection: 'column', justifyContent: 'space-around', paddingLeft: 10 }} >
                    <Text style={[styling.textStyleSalesman, { fontSize: 11 }]}>Last Month</Text>
                    <Text style={[styling.textStyleSalesman, { fontSize: 11 }]}>Avg. Last 3 Month</Text>
                  </View>
                )}
              </>
            ))}
          </Table>
        </ScrollView>
      </View>
      <View style={styling.rowContainer}>
        <ScrollView horizontal={true} bounces={false}>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <Row data={['QTY']} widthArr={[180]} style={[styling.boxHead, { borderWidth: 0.5, borderColor: 'white' }]} textStyle={{ ...styling.textStyle, color: '#64748b' }} />
              <Row data={['Value']} widthArr={[260]} style={[styling.boxHead, { borderWidth: 0.5, borderColor: 'white' }]} textStyle={{ ...styling.textStyle, color: '#64748b' }} />
            </View>
            <Table borderStyle={{ borderWidth: 1, borderColor: BORDER_COLOR }}>
              <Row
                data={coreTableHead.tableHead}
                widthArr={coreTableHead.widthArr}
                style={styling.boxHead}
                textStyle={{ ...styling.textStyle, color: '#64748b' }}
              />
            </Table>
            <ScrollView
              ref={rightRef}
              scrollEventThrottle={16}
              bounces={false}
              style={{ marginTop: -1 }}
              onScroll={(e) => {
                const { y } = e.nativeEvent.contentOffset;
                leftRef.current?.scrollTo({ y, animated: false });
              }}
            >
              <Table borderStyle={{ borderWidth: 1, borderColor: BORDER_COLOR }}>
                {tableDataCore.map((rowData: any, k: number) => {
                  const data = rowData?.slice(0, (rowData.length - 2));
                  console.log("oke", rowData[6]?.currency_id);

                  const tableCollapseLastMonth: any = [
                    rowData[rowData?.length - 2].qty_tgt,
                    rowData[rowData?.length - 2].qty_real,
                    rowData[rowData?.length - 2].qty_percentage,
                    rowData[rowData?.length - 2].value_tgt,
                    rowData[rowData?.length - 2].value_real,
                    rowData[rowData?.length - 2].value_percentage,
                  ];
                  const tableCollapseLastThreeMonth: any = [
                    rowData[rowData?.length - 1].qty_tgt,
                    rowData[rowData?.length - 1].qty_real,
                    rowData[rowData?.length - 1].qty_percentage,
                    rowData[rowData?.length - 1].value_tgt,
                    rowData[rowData?.length - 1].value_real,
                    rowData[rowData?.length - 1].value_percentage,
                  ];
                  return (
                    <>
                      <Row
                        key={k}
                        data={data}
                        widthArr={coreTableHead.widthArr}
                        style={k % 2 ? styling.rowEven : styling.rowOdd}
                        textStyle={styling.textStyle}
                      />
                      {isCollapse === (k + 1) && (
                        <View style={{ height: 65 }}>
                          <Row
                            key={k}
                            data={tableCollapseLastMonth}
                            widthArr={coreTableHead.widthArr}
                            style={styling.rowOdd}
                            textStyle={styling.textStyle}
                          />
                          <Row
                            key={k}
                            data={tableCollapseLastThreeMonth}
                            widthArr={coreTableHead.widthArr}
                            style={styling.rowEven}
                            textStyle={styling.textStyle}
                          />
                        </View>
                      )}
                    </>
                  )
                })}
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </View>
  );
}